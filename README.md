# ddosrussia

Popular (already banned) ddosing website moved to Electron app, so it does not care about CORS.

### PRE-REQUISITES

- node.js and npm

### DEVELOPMENT

- npm install
- npm start

### BUILD

- npm install
- npm run package

### PACKAGES

- Linux DEV and RPM version is in out/make directory

### RUN ON LINUX

- `sudo dpkg -i builds/linux/x64/ddosrussion_1.0.0_amd64.deb`
- ddosrussia

### RUN ON MAC
- extract the zip from builds/ddosrussia-darwin-x64.zip
- just run ddosrussia

## WINDOWS
- feel free to leave MR with build until I did it

### DONATE TO UKRAINE ARMY

https://savelife.in.ua/donate/

SWIFT
Company Name

CO "INTERNATIONAL CHARITABLE FOUNDATION "COME BACK ALIVE"

IBAN Code (Euro)                                               IBAN Code (US Dollars)     
UA093052990000026004025029786.              UA173052990000026009035028620   

Name of the Bank
JSC CB "PRIVATBANK", 1D HRUSHEVSKOHO STR., KYIV, 01001, UKRAINE

Bank SWIFT-Code
PBANUA2X

Company address
32 BOHDANA KHMELNYTSKOHO STR., KYIV, 01054, UKRAINE (м. Київ, вул. Богдана Хмельницького б.32, кв.41)

Purpose of payment
Charitable donation to Ukrainian military

Bitcoin Wallet
bc1qkd5az2ml7dk5j5h672yhxmhmxe9tuf97j39fm6

You can support our team:
Patreon: https://www.patreon.com/savelife_in_ua

Adress:
32, Bohdana Hmelnytskoho St., office 41, Kyiv
the 1st level, entrance through the arch

Phone:
+38(044)338-33-38, +38(068)500-88-00
Mon.-Fri.: 10:00-19:00

